package main

import (
	"testing"
)

// TestEmptyReq tests parseStanRequest with empty data
func TestEmptyReq(t *testing.T) {
	_, err := parseStanRequest([]byte(``))
	if err == nil {
		t.Error("No error on empty JSON")
	}
}

// TestEmptyReq tests parseStanRequest with wrongly formed JSON
func TestWrongJson(t *testing.T) {
	_, err := parseStanRequest([]byte(`{"}`))
	if err == nil {
		t.Error("No error on wrong JSON")
	}
}

var testData = []byte(`{ "payload" : [
	{ 
		"drm" : true, 
		"episodeCount" : 3, 
		"image": {
               "showImage": "ImageURL"
           },
		"title": "Title"
	}
]}`)

// TestRequestParsing tests that all request fields are filled properly
func TestRequestParsing(t *testing.T) {

	req, err := parseStanRequest(testData)
	if err != nil {
		t.Error(err)
		return
	}

	if len(req.Payload) != 1 {
		t.Error(
			"For", "payload field length",
			"expected", 1,
			"got", len(req.Payload),
		)
		return
	}

	if req.Payload[0].Drm != true {
		t.Error(
			"For", "drm field",
			"expected", true,
			"got", req.Payload[0].Drm,
		)
	}
	if req.Payload[0].EpisodeCount != 3 {
		t.Error(
			"For", "episodeCount field",
			"expected", 3,
			"got", req.Payload[0].EpisodeCount,
		)
	}
	if req.Payload[0].Title != "Title" {
		t.Error(
			"For", "title field",
			"expected", "Title",
			"got", req.Payload[0].Title,
		)
	}
	if req.Payload[0].Image.ShowImage != "ImageURL" {
		t.Error(
			"For", "image.showImage field",
			"expected", "ImageURL",
			"got", req.Payload[0].Image.ShowImage,
		)
	}
}

// TestResopnseForming tests that the response fields matche to the request fields
func TestResopnseForming(t *testing.T) {
	req, err := parseStanRequest(testData)
	if err != nil {
		t.Error(err)
		return
	}
	resp := formStanResponse(req)
	if len(resp.Response) != 1 {
		t.Error(
			"For", "response field length",
			"expected", 1,
			"got", len(resp.Response),
		)
		return
	}
	if req.Payload[0].Title != resp.Response[0].Title {
		t.Error(
			"For", "title field",
			"expected", req.Payload[0].Title,
			"got", resp.Response[0].Title,
		)
	}
	if req.Payload[0].Slug != resp.Response[0].Slug {
		t.Error(
			"For", "slug field",
			"expected", req.Payload[0].Slug,
			"got", resp.Response[0].Slug,
		)
	}
	if req.Payload[0].Image.ShowImage != resp.Response[0].Image {
		t.Error(
			"For", "image field",
			"expected", req.Payload[0].Image.ShowImage,
			"got", resp.Response[0].Image,
		)
	}
}

// TestRequestDrmCheck tests that the requirement of drm=true is
// satisfied
func TestRequestDrmCheck(t *testing.T) {
	req, err := parseStanRequest([]byte(`{ "payload" : [ {"drm" : true, "episodeCount" : 1} ]}`))
	if err != nil {
		t.Error(err)
		return
	}
	resp := formStanResponse(req)
	if len(resp.Response) != 1 {
		t.Error(
			"For", "response field length",
			"expected", 1,
			"got", len(resp.Response),
		)
		return
	}

	req, err = parseStanRequest([]byte(`{ "payload" : [ {"drm" : false, "episodeCount" : 1} ]}`))
	resp = formStanResponse(req)
	if len(resp.Response) != 0 {
		t.Error(
			"For", "response field length",
			"expected", 0,
			"got", len(resp.Response),
		)
		return
	}
}

// TestRequestEpisodeCountCheck check that the requirement of episodeCount > 0 is
// satisfied
func TestRequestEpisodeCountCheck(t *testing.T) {
	req, err := parseStanRequest([]byte(`{ "payload" : [ {"episodeCount" : 1, "drm" : true} ]}`))
	if err != nil {
		t.Error(err)
		return
	}
	resp := formStanResponse(req)
	if len(resp.Response) != 1 {
		t.Error(
			"For", "response field length",
			"expected", 1,
			"got", len(resp.Response),
		)
		return
	}

	req, err = parseStanRequest([]byte(`{ "payload" : [ {"episodeCount" : 0, "drm" : true} ]}`))
	resp = formStanResponse(req)
	if len(resp.Response) != 0 {
		t.Error(
			"For", "response field length",
			"expected", 0,
			"got", len(resp.Response),
		)
		return
	}
}
