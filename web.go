// a solution for the StreamCo Coding Challenge
// receives JSON requests http://streamco-coding-challenge.herokuapp.com/samples/request.json
// answers JSON response http://streamco-coding-challenge.herokuapp.com/samples/response.json

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"
)

// PayloadImage represents the image field structure
type PayloadImage struct {
	ShowImage string
}

// PayloadItem represents the required by StreamCo Coding Challenge fields
type PayloadItem struct {
	Drm          bool
	EpisodeCount int
	Image        PayloadImage
	Slug         string
	Title        string
}

// StanRequest represents the required by StreamCo Coding Challenge request
type StanRequest struct {
	Payload []PayloadItem
}

// StanResponseItem represents the required by StreamCo Coding Challenge
// response fields
type StanResponseItem struct {
	Image string `json:"image"`
	Slug  string `json:"slug"`
	Title string `json:"title"`
}

// StanResponse represents the required by StreamCo Coding Challenge response
type StanResponse struct {
	Response []StanResponseItem `json:"response"`
}

// parseStanRequest unmarshals JSON data to *StanRequest
func parseStanRequest(data []byte) (*StanRequest, error) {
	var req StanRequest
	err := json.Unmarshal(data, &req)
	if err != nil {
		return nil, err
	}
	return &req, nil
}

// readBody reads the request body JSON data
func readBody(req *http.Request) ([]byte, error) {
	if req.Body == nil {
		return nil, errors.New("empty body")
	}
	body, err := ioutil.ReadAll(req.Body)
	if err != nil {
		return nil, err
	}
	req.Body.Close()
	return body, nil
}

// formStanResponse gets *StanRequest and returns a pointer to StanResponse which consists from
// the StanRequest items which have drm and more than one episode
func formStanResponse(req *StanRequest) *StanResponse {
	var cnt int
	for _, item := range req.Payload {
		if item.Drm && item.EpisodeCount > 0 {
			cnt++
		}
	}
	var response StanResponse
	response.Response = make([]StanResponseItem, cnt)
	var idx int
	for _, item := range req.Payload {
		if item.Drm && item.EpisodeCount > 0 {
			response.Response[idx].Image = item.Image.ShowImage
			response.Response[idx].Slug = item.Slug
			response.Response[idx].Title = item.Title
			idx++
		}
	}
	return &response
}

// formHttpResponse forms the required answer on the request from Stan
func formHttpResponse(req *StanRequest, resp http.ResponseWriter) error {
	answer := formStanResponse(req)
	output, err := json.Marshal(answer)
	if err != nil {
		return err
	}
	_, err = resp.Write(output)
	return err
}

func rootHandler(resp http.ResponseWriter, req *http.Request) error {
	body, err := readBody(req)
	if err != nil {
		return err
	}
	stanRequest, err := parseStanRequest(body)
	if err != nil {
		return err
	}
	return formHttpResponse(stanRequest, resp)
}

// formHttpError forms JSON error answer and writes it to http resopnse
func formHttpError(err error, resp http.ResponseWriter) {
	resp.WriteHeader(http.StatusBadRequest)
	var buffer bytes.Buffer
	buffer.WriteString("{ \"error\": \"Could not decode request: ")
	// Quote the error string to prevent JSON syntax errors
	quotedStr := strconv.Quote(err.Error())
	buffer.WriteString(quotedStr[1 : len(quotedStr)-1])
	buffer.WriteString("\"}")
	log.Println(err)
	resp.Write(buffer.Bytes())
}

// makeRequestHandler makes a http handler wrapper function which provides
// application/json contect type header and
// a JSON error response in a case of an error
func makeRequestHandler(fn func(http.ResponseWriter, *http.Request) error) http.HandlerFunc {

	return func(resp http.ResponseWriter, req *http.Request) {
		resp.Header().Set("Content-Type", "application/json; charset=utf-8")
		err := fn(resp, req)
		if err != nil {
			formHttpError(err, resp)
		}
	}
}

func main() {
	http.HandleFunc("/", makeRequestHandler(rootHandler))
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	err := http.ListenAndServe(":"+port, nil)
	if err != nil {
		panic(err)
	}
}
